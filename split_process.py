import glob
import os
import random


name_of_computer = 'jf'
name_dataset = "daruma_labelled"
current_dir = f'/home/{name_of_computer}/setup_train/dataset/{name_dataset}'
path_txt = f'/home/{name_of_computer}/setup_train/dataset/{name_dataset}'
print(current_dir)

image_dir = os.path.join(current_dir, 'datasets')
current_dir = os.path.expanduser(image_dir)

percentage_train = 70

train_file = os.path.join(path_txt, 'train.txt')
test_file = os.path.join(path_txt, 'test.txt')

image_paths = glob.glob(os.path.join(current_dir, "*.jpg"))

random.shuffle(image_paths)

# Calculate the split index
split_index = int(len(image_paths) * (percentage_train / 100.0))

# Split into train and test sets
train_paths = image_paths[:split_index]
test_paths = image_paths[split_index:]

file_train = open(train_file, 'w')
file_test = open(test_file, 'w')

for path in train_paths:
    title, ext = os.path.splitext(os.path.basename(path))
    file_train.write(f"{current_dir}" + "/" + title + '.jpg' + "\n")

for path in test_paths:
    title, ext = os.path.splitext(os.path.basename(path))
    file_test.write(f"{current_dir}" + "/" + title + '.jpg' + "\n")

file_train.close()
file_test.close()
