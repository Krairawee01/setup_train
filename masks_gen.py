import os
import cv2

name_of_computer = 'jf'
path_dir =f"/home/{name_of_computer}/setup_train/data"

def convert_masks(image_paths, masks):
    # Suppress libpng warning
    cv2.setLogLevel(0)

    # Create the mask directories if they don't exist
    for obj_name, mask_dir in masks.items():
        os.makedirs(mask_dir, exist_ok=True)

    # Iterate over the image paths dictionary
    for obj_name, image_dir in image_paths.items():
        mask_dir = masks.get(obj_name)
        if not mask_dir:
            continue

        # Get the list of image files in the object directory
        image_files = [filename for filename in os.listdir(image_dir) if filename.endswith('.png')]

        # Process each image in the image directory
        for i, filename in enumerate(image_files):
            image_path = os.path.join(image_dir, filename)
            convert_mask(image_path, mask_dir, i)

def convert_mask(image_path, mask_dir, count):
    # Load the image
    image = cv2.imread(image_path, cv2.IMREAD_COLOR)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    _, mask = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)

    mask = cv2.bitwise_not(mask)

    masked_image = image.copy()
    masked_image[mask == 0] = [0, 0, 0]

    masked_image[mask > 0] = [255, 255, 255]

    filename = f"{count}.png"
    output_path = os.path.join(mask_dir, filename)
    cv2.imwrite(output_path, masked_image)

    print(f"Processed: {filename}")

if __name__ == '__main__':
    image_paths = {
        "obj1": f"{path_dir}/daruma/images"
        # "obj2": f"{path_dir}/<your_obj>/images",
        # "obj3": f"{path_dir}/<your_obj>/images",
    }

    masks = {
        "obj1": f"{path_dir}/daruma/masks"
        # "obj2": f"{path_dir}/<your_obj>/masks"
        # "obj3": f"{path_dir}/<your_obj>/masks",
    }

    convert_masks(image_paths, masks)
