import os
import sys
import subprocess
import time

name_of_computer = 'jf'
path_dir = f"/home/{name_of_computer}/setup_train" # must change name path to your computer
cfg = 'daruma-yolov7-tiny' # can change name of cfg
name_pretrain = 'yolov7-tiny.conv.87' # change with pre trained weight you want
pre_weight = f'{name_pretrain}' # change with pretrained weight 
dataset_dir = f"{path_dir}/dataset"
images_dir = f"{path_dir}/images"
mask_dir = f"{path_dir}/masks"
name_data_darknet = "daruma"



def train_darknet():
    package_path = os.path.expanduser(path_dir)

    print("Start Data generation and Split text.txt,train.txt ...\n")
    # convertmask image to binary masks
    masks_gen__path = os.path.join(package_path, "masks_gen.py")
    start_time = time.time()
    subprocess.run(["python3", masks_gen__path])
    end_time = time.time()
    print("masks_gen.py execution time:", end_time - start_time)
    


    # data gen
    data_gen_script_path = os.path.join(package_path,"datagen.py")
    start_time = time.time()
    subprocess.run(["python3",data_gen_script_path])
    end_time = time.time()
    print(f"Datagen.py execution time: {end_time-start_time}")

    # create classes.txt
    classes_file_path = os.path.join(f"{package_path}/dataset/daruma_labelled", "datasets", "classes.txt")
    with open(classes_file_path, "w") as f:
        obj = ['daruma','battery','lightbulb','padlock']
        for i in obj:
            f.write(f"{i}\n")

    

    # process split train and test
    process_script_path = os.path.join(package_path, "split_process.py")
    start_time = time.time()
    subprocess.run(["python3", process_script_path])
    end_time = time.time()
    print("split_process.py execution time:", end_time - start_time)
    


    print("Data generation and Split test.txt,train.txt complete ...\n")

    print("Start Training with Darknet")
    darknet_path = f"/home/{name_of_computer}/darknet"

    os.chdir(darknet_path)

    # create obj.data file
    with open(f"data/{name_data_darknet}.data", "w") as f:
        f.write(f"classes = 4\n")
        f.write(f"train = {dataset_dir}/daruma_labelled/train.txt\n")
        f.write(f"valid = {dataset_dir}/daruma_labelled/test.txt\n")
        f.write(f"names = data/{name_data_darknet}.names\n")
        f.write(f"backup = backup/\n")

    # create obj.names file
    with open(f"data/{name_data_darknet}.names", "w") as f:
        obj = ['daruma','battery','lightbulb','padlock']
        for i in obj:
            f.write(f"{i}\n")


    start_time = time.time()
    cmd = f"./darknet detector train data/{name_data_darknet}.data cfg/{cfg}.cfg {pre_weight} -map"
    os.system(cmd)
    end_time = time.time()
    print("Training execution time:", end_time - start_time)

    print("Training completed.")


if __name__ == "__main__":
    try:
        train_darknet()
    except Exception as e:
        print("Error occurred during training:", str(e))
        sys.exit(1)

